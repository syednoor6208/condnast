import express from "express";
const app = express();
app.use(express.json());
import { getEveryThing } from "./everything";
const PORT = process.env.PORT || 8000;

// app.post("/api/login", loginService);
app.get("/api/news", getEveryThing);
app.listen(PORT, (): void => {
  console.log(`Server Running http://localhost:${PORT}`);
});
