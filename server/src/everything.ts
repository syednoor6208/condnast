import { Request, Response } from "express";
import dotenv from "dotenv";
import NewsAPI from "newsapi";
dotenv.config();
const { APIKey } = process.env;
export async function getEveryThing(req: Request, res: Response) {
  try {
    const { contains, from_date, to_date, page } = req.query;
    const currentDate = new Date().toISOString().slice(0, 10);
    const newsApi = new NewsAPI(APIKey);
    const result = await newsApi.v2.everything({
      language: "en",
      q: contains,
      domains: "bbc.co.uk",
      from: from_date ? from_date : currentDate,
      to: to_date ? to_date : currentDate,
      page: page ? parseInt(`${page}`) : 1,
      pageSize: 20,
    });
    res.status(200).json(result);
  } catch (error: any) {
    res.status(400).json({ success: false, message: error.message });
  }
}
