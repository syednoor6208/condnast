import React, { useContext } from "react";
import { useQuery } from "react-query";
import { MyContext } from "./myContext";
import { getNews } from "./service";
export default React.memo(function NewsRoom() {
  const { contains, from_date, to_date, setTotalRecords, page } =
    useContext(MyContext);
  const { isLoading, data, isFetching, isError, error } = useQuery(
    ["news", { contains, from_date, to_date, page }],
    getNews,
    {
      keepPreviousData: true,
      refetchInterval: false,
      refetchOnWindowFocus: false,
      refetchOnMount: false,
      onSuccess: (result: any) => {
        setTotalRecords(result.totalResults);
      },
    }
  );
  if (isLoading || isFetching) {
    return (
      <div className="isLoading">
        <p>Please wait loading</p>
      </div>
    );
  }
  if (isError) {
    return (
      <div className="isLoading" style={{ color: "red" }}>
        <p>{(error as any)?.response?.data?.message}</p>
      </div>
    );
  }
  if (data?.articles?.length === 0) {
    return (
      <div className="isLoading">
        <p>No Result found.!</p>
      </div>
    );
  }
  return (
    <>
      {data?.articles.map((item: any) => (
        <div key={item.url} className="news-container">
          <div className="news">
            <div className="news-preview">
              <img src={item.urlToImage} alt={item.urlToImage} />
            </div>
            <div className="news-info">
              <h4>{item.title}</h4>
              <h6>{item.description}</h6>
              <a
                className="btn"
                href={item.url}
                target="_blank"
                rel="noreferrer"
              >
                Read more
              </a>
            </div>
          </div>
        </div>
      ))}
    </>
  );
});
