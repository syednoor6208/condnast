import { createContext } from "react";
export interface contextParams {
  page: number;
  totalRecords: number;
  contains: string;
  from_date: string;
  to_date: string;
  setPage: Function;
  setTotalRecords: Function;
  setContains: Function;
  setFromDate: Function;
  setToDate: Function;
}
export const currentDate = new Date().toISOString().substr(0, 10);
const cnt: contextParams = {
  page: 1,
  totalRecords: 0,
  contains: "",
  from_date: currentDate,
  to_date: currentDate,
  setPage: (pageNo: number) => {},
  setTotalRecords: (total: number) => {},
  setContains: (contains: string) => {},
  setFromDate: (date: string) => {},
  setToDate: (date: string) => {},
};
export const MyContext = createContext(cnt);
