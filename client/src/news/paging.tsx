import React, { useContext } from "react";
import { MyContext } from "./myContext";
export default React.memo(function Paging() {
  const { setPage, totalRecords, page } = useContext(MyContext);
  const totalPages = Math.ceil(totalRecords / 20);
  function firstRecord() {
    setPage(1);
  }
  function previousRecord() {
    setPage(page - 1 < 0 ? 0 : page - 1);
  }
  function nextRecord() {
    setPage(page + 1 > totalPages ? totalPages : page + 1);
  }
  function lastRecord() {
    setPage(totalPages);
  }

  return (
    <div className="pagination">
      <span title="First Record" onClick={firstRecord}>
        ❮❮
      </span>
      <span title="Previous Record" onClick={previousRecord}>
        ❮
      </span>
      <span className="page-total">
        {page}/{totalPages}
      </span>
      <span title="Next Record" onClick={nextRecord}>
        ❯
      </span>
      <span title="Last Record" onClick={lastRecord}>
        ❯❯
      </span>
    </div>
  );
});
