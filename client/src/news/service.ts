import axios from "axios";
interface getNewParams {
  from_date: string;
  to_date: string;
  contains: string;
  page: number;
}
export async function getNews({ queryKey }: any) {
  const params: getNewParams = queryKey[1];
  const { data } = await axios.get("/api/news", {
    params: params,
  });
  return data;
}
