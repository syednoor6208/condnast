import React, { useContext, memo, useState } from "react";
import { MyContext, currentDate } from "./myContext";

export default memo(function Filter() {
  const { setFromDate, setToDate, setContains, setPage } =
    useContext(MyContext);
  const [fromD, setFromD] = useState(currentDate);
  const [toD, setToD] = useState(currentDate);
  const [cont, setCont] = useState("");
  function onChangeDateHandler(e: React.ChangeEvent<HTMLInputElement>) {
    const { value, name } = e.target;
    if (name === "fromDate") {
      setFromD(value);
    } else if (name === "toDate") {
      setToD(value);
    } else {
      setCont(value);
    }
  }
  function searchHandler() {
    setFromDate(fromD);
    setToDate(toD);
    setContains(cont);
    setPage(1);
  }

  return (
    <div className="form-control col-12">
      <div className="col-2">
        <label>From Date</label>
        <input
          type="date"
          name="fromDate"
          defaultValue={fromD}
          max={currentDate}
          onChange={onChangeDateHandler}
        />
      </div>
      <div className="col-2">
        <label>To Date</label>
        <input
          type="date"
          name="toDate"
          defaultValue={toD}
          max={currentDate}
          onChange={onChangeDateHandler}
        />
      </div>
      <div className="col-3">
        <label>Search</label>
        <input
          type="text"
          defaultValue={cont}
          name="contains"
          onChange={onChangeDateHandler}
        />
      </div>
      <div className="col-1">
        <button className="search" onClick={searchHandler}>
          Search
        </button>
      </div>
    </div>
  );
});
