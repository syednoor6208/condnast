import { useReducer } from "react";
import { MyContext, currentDate } from "./myContext";
import Filter from "./filter";
import NewsRoom from "./newsRoom";
import Pagination from "./paging";
function reducer(
  state: any,
  { type, payload }: { type: string; payload: any }
) {
  switch (type) {
    case "page":
      return {
        ...state,
        page: payload,
      };
    case "totalRecords":
      return {
        ...state,
        totalRecords: payload,
      };
    case "fromDate":
      return {
        ...state,
        from_date: payload,
      };
    case "toDate":
      return {
        ...state,
        to_date: payload,
      };
    default:
      return {
        ...state,
        contains: payload,
      };
  }
}
export default function News() {
  const [state, dispatch] = useReducer(reducer, {
    page: 1,
    totalRecords: 0,
    contains: "",
    from_date: currentDate,
    to_date: currentDate,
  });
  const dispatches = {
    setPage(pageNo: number) {
      dispatch({ type: "page", payload: pageNo });
    },
    setTotalRecords(total: number) {
      dispatch({ type: "totalRecords", payload: total });
    },
    setContains(contains: string) {
      dispatch({ type: "contains", payload: contains });
    },
    setFromDate(date: string) {
      dispatch({ type: "fromDate", payload: date });
    },
    setToDate(date: string) {
      dispatch({ type: "toDate", payload: date });
    },
  };
  return (
    <MyContext.Provider value={{ ...state, ...dispatches }}>
      <h2>Conde Nast News feeds</h2>
      <Filter />
      <NewsRoom />
      <Pagination />
    </MyContext.Provider>
  );
}
